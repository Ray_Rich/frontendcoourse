console.log("task 16: ");
task16();
console.log();
console.log("task 17: ");
task17();
console.log();
console.log("task 18: ");
task18();
console.log();
console.log("task 19: ");
task19();
console.log();
console.log("task 20: ");
task20();

function getRandomNumber() {
    let randomNumber = Math.round(Math.random() * 50);
    return randomNumber;
}

function createMatrixArray(rows, columns) {
    let array = new Array();
        for (let i = 0; i < rows; i++) {
            array[i] = new Array();
            for (let j = 0; j < columns; j++){
            array[i][j] = getRandomNumber();
          }
        }
        return array;
}

function showElements(array){
    console.log("Init array is: ");
    for(let i = 0; i < array.length; i++){
        for(let j = 0; j < array[i].length; j++){
            if (array[i][j] / 10 < 1) {
                array[i][j] = " " + array[i][j];
            }
            
        }
        console.log(array[i].toString().split(",").join(" "));
    }
}

function task16() {    
    showElements(createMatrixArray(getRandomNumber(), getRandomNumber()));
}

function task17() {
    function countElements() {
        let rows = getRandomNumber(), columns = getRandomNumber();
        let array = createMatrixArray(rows, columns);
        let count = 0;
        for (let i = 1; i < rows - 1; i++) {
            for (let j = 1; j < columns - 1; j++) {
                if(array[i][j] > array[i-1][j] && array[i][j] > array[i+1][j] && array[i][j] > array[i][j-1] && array[i][j] > array[i][j+1]) {
                    count++;
                }
            }
        }
        showElements(array);
        return count;
    }
    console.log("Count = " + countElements());    
}

function task18() {    
    let rows = getRandomNumber(), columns = getRandomNumber();
    let array = createMatrixArray(rows, columns);
    let diagonalsArr = [];
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < columns; j++) {
            if (i == j) {
                diagonalsArr.push(array[i][j]);
            }
        }
    }
    showElements(array);
    console.log("Main diagonals of matrix: " + diagonalsArr);
}

function task19() {
    let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
    let x = 0, y = 0;

    let direction = 'right';
    let numberPosition = 0;
    let innerDimension;
    let dimension = 5;
    let oldPosition;

    let matrix = createMatrixArray(5, 5);
    fillMatrix();

    console.log(matrix);

    function fillMatrix() {
        if (!numbers.length) return;
        let num = numbers.shift();
        matrix[x][y] = num;
        ++numberPosition;

        if (innerDimension === 0) return;
        if (innerDimension === 1) {
            getNextDirection();
            setNextPosition();
            innerDimension = 0;
            fillMatrix();
            return;
        }

        if (numberPosition === innerDimension || numberPosition === dimension) {
            getNextDirection();
            setNextPosition();
            oldPosition = numberPosition;
            innerDimension = dimension - 1;
            fillMatrix();
            return;
        }

        if (numberPosition === oldPosition + innerDimension || numberPosition === oldPosition + (innerDimension * 2)) {
            if (numberPosition === oldPosition + (innerDimension * 2)) {
                innerDimension = innerDimension - 1;
                oldPosition = numberPosition;
            }
            getNextDirection();
            setNextPosition();
            fillMatrix();
            return;
        }

        setNextPosition();
        fillMatrix();
    }

    function setNextPosition() {
        if (direction == 'right') {
            y = y + 1;
        }

        if (direction == 'down') {
            x = x + 1;
        }

        if (direction == 'left') {
            y = y - 1;
        }

        if (direction == 'top') {
            x = x - 1;
        }
    }

    function getNextDirection() {
        if (direction === 'right') {
            direction = 'down';
            return;
        }

        if (direction === 'down'){
            direction = 'left';
            return;
        }

        if (direction === 'left') {
            direction =  'top';
            return;
        }

        if (direction === 'top') {
            direction = 'right';
        }
    }
}

function task20() {
    let matrixA = createMatrixArray(3, 3);
    let matrixB = createMatrixArray(3, 3);

    function multiplyMatrix (A,B) {
    let rowsA = A.length, colsA = A[0].length,
        rowsB = B.length, colsB = B[0].length,
        C = [];
    if (colsA != rowsB) return false;
    for (let i = 0; i < rowsA; i++) C[ i ] = [];
    for (let k = 0; k < colsB; k++) {
        for (let i = 0; i < rowsA; i++) {
            let t = 0;
            for (let j = 0; j < rowsB; j++) t += A[ i ][j]*B[j][k];
            C[ i ][k] = t;
        }
    }
    return C;
    }
    console.log("Multiply first matrix: ");
    console.log(matrixA);
    console.log("and second matrix: ");
    console.log(matrixB);
    console.log(" = ");
    console.log(multiplyMatrix(matrixA, matrixB));
}