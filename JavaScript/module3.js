task11();
task12();
task13();
task14();
task15();

function getRandomNumber() {
    let randomNumber = Math.round(Math.random() * 30 + 5);
    return randomNumber;
}

function setRandomNumbersToArray(arraySize) {
    let array = [];
    for (let i = 0; i < arraySize; i++) {
        array[i] = getRandomNumber();
    }
    return array;
}

function task11() {
    let array = setRandomNumbersToArray(getRandomNumber());
    let arrayReverse = [];
    for (let i = array.length - 1; i >= 0; i--) {
        arrayReverse.push(array[i]);        
    }
    console.log("Initial array = " + array);
    console.log("Array after reverse = " + arrayReverse);
    return arrayReverse;
}

function task12() {
    let array12 = setRandomNumbersToArray(getRandomNumber());
    let count = 0;
    for (let i = 0; i < array12.length; i++) {
        if(array12[i] % 2 === 1) {
            count++;
        }
    }
    console.log("Number of odd array elements: " + count + ". Array is " + array12);
    return count;
}

function task13() {
    let array13 = setRandomNumbersToArray(getRandomNumber());
    let halfArray = Math.floor(array13.length / 2);
    let halfArrayWithModulo = halfArray + array13.length % 2;
    let newArray = [];
    for (let i = 0; i < halfArrayWithModulo; i++) {    
        newArray[i] = array13[halfArrayWithModulo + i];
    }
    if (array13.length % 2 !== 0) {
        newArray[halfArray] = array13[halfArray];
    }
    for (let i = 0; i < halfArray; i++) {
        newArray.push(array13[i]);

    }
    console.log("Initial array is " + array13);
    console.log("Array after changing places: " + newArray);
    return newArray;
}

function task14() {
    let array14 = setRandomNumbersToArray(getRandomNumber());
    console.log("Initial array before Bubble sort: " + array14);
    for (let i = 0, endI = array14.length - 1; i < endI; i++) {
        for (let j = 0, endJ = endI - i; j < endJ; j++) {
            if (array14[j] > array14[j + 1]) {
                let temp = array14[j];
                array14[j] = array14[j + 1];
                array14[j + 1] = temp;
            }
        }
    }
    console.log("Array after bubble sort: " + array14);
    return array14;    
}

function task15() {
    let array15 = setRandomNumbersToArray(getRandomNumber());
    console.log("Array before Select sort: " + array15);
    let length = array15.length;
    for (let i = 0; i < length - 1; i++) {
        let min = i;
        for (let j = i + 1; j < length; j++) {
           if (array15[j] < array15[min]) min = j; 
        } 
        let temp = array15[min];
        array15[min] = array15[i];
        array15[i] = temp;
    }
    console.log("Array after Select sort: " + array15);   

    let array16 = setRandomNumbersToArray(getRandomNumber());
    console.log("Array before Insert sort: " + array16);
    let len = array16.length;
    for (let i = 0; i < len; i++) {
        let v = array16[i], j = i - 1;
        while (j >= 0 && array16[j] > v) {
        array16[j + 1] = array16[j];
        j--;
        }
        array16[j + 1] = v;
    }                    
    console.log("Array after Insert sort: " + array16); 
}