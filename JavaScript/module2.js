task6();
task7();
task8();
task9();
task10();

function generateRandomNumber(maxValue) {
    return Math.round(Math.random() * maxValue + 4);
}

function task6() {
    let sum = count = 0;
    for (let i = 1; i < 100; i++) {
        if (i % 2 === 0) {
            sum += i;
            count++;
        }
    }
    console.log("Summary of even digits = " + sum);
    console.log("Number of even digits = " + count);
}

function task7() {
    function isNumberNatural(number) {
        return number > 0; 
    }
    function isNumberSimple(number) { 
        let isSimple = true;
        if(isNumberNatural(number)) {                      
            if (number === 1) {
                return isSimple;
            }
            for (let i = 2; i < number; i++) {
                if (number % i === 0 && number !== i) {
                    return false;
                }
            }
            return isSimple;
        } else {
            return false;
        }   
    }
    let result;
    let randomNumber = generateRandomNumber(1000);
    if(isNumberSimple(randomNumber)) {
        result = "The number " + randomNumber + " is simple.";
    } else {
        result = "The number " + randomNumber + " isn't simple.";
    }
    console.log(result);
    
    function squareOfNumber(number) {
        let prev;
        let next = number;
        do {
            prev = next;
            next = (prev + number / prev) / 2;
        } while ((prev - next) > 0);
        return Math.round(next);
    } 

    function squareOfNumberBinary(number) {
        let left = 1, right = number;
        while (left <= number) {   
            let middle = (left + right) / 2;
            let end = Math.round(middle * middle);
            if (number == end) {
                return Math.round(middle);
            } else if (number < end) {
                right = middle + 1;
            } else {
                left = middle - 1;
            }
        }
    }

    if(isNumberNatural(randomNumber)) {
        console.log("Square of number " + randomNumber + ": " + squareOfNumber(randomNumber));
        console.log("Square of number " + randomNumber + ": " + squareOfNumberBinary(randomNumber) + "(binary way).");
    } else {
        console.log("You should put other number.");
    }
}

function task8() {
    let facNumber = generateRandomNumber(30);
    
    //Way with recursion
    function factorial(number) {
        if (number === 0) return 1;
        return factorial(number - 1) * number;
    }
    console.log("Factorial of " + facNumber + " = " + factorial(facNumber) + "(recursion way)");

    //Cycle way
    function factorialCycle(number) {
        let result = 1;
        for (let i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }
    console.log("Factorial of " + facNumber + " = " + factorialCycle(facNumber) + "(cycle way)");
}

function task9() {
    let calculationNumber = generateRandomNumber(1000000);
    function calcDigitsSum(a) {
        let result = 0;
        while(a !== 0) {
            result += a % 10;
            a = Math.floor(a / 10);
        }
        return result;   
    }
    console.log("Summary of digits " + calculationNumber + " = " + calcDigitsSum(calculationNumber));
}

function task10() {
    function convertNumberToString(number) {
        return number.toString();
    }
    function checkNumberLength(string) {
        if (string.length >= 100) {
            alert("You've entered too big number. Try again with less number.");
        }
    }    
    function arrayJoin(array) {
        return array.join("");
    }
    
    let genericNumber = generateRandomNumber(10000000000000000);
    let numberToString = convertNumberToString(genericNumber);
    checkNumberLength(numberToString);

    //Way without strings
    function strangeReverseDigits(number) {
        let revNumber = 0;
        while (number) {
            revNumber *= 10;
            revNumber += number % 10;
            number = Math.floor(number / 10);
        }
        return revNumber;
    }
    console.log("Mirror reflection of " + genericNumber + " = " + strangeReverseDigits(genericNumber) + " (without strings).");

    //Way without array
    function reverseDigits(a) {
        let temp = 0, result = "";
        while(a !== 0) {
            temp = a % 10;
            a = Math.floor(a / 10);
            result += temp;
        }
        return result;   
    }
    console.log("Mirror reflection of " + genericNumber + " = " + reverseDigits(genericNumber) + " (without arrays).");

    //Unshift way
    let array = [];
    for (let i = 0; i < numberToString.length; i++) {
        array.unshift(numberToString.charAt(i));
    }
    console.log("Mirror reflection is " + arrayJoin(array) + " (unshift way).");

    //Push way
    let arraySecond = [];
    for (let i = numberToString.length - 1; i >= 0; i--) {
        arraySecond.push(numberToString.charAt(i));
    }
    console.log("Mirror reflection is " + arrayJoin(arraySecond) + " (push way).");

    //Reverse way
    let arrayReverse = [];
    for (let i = 0; i < numberToString.length; i++) {
        arrayReverse.push(numberToString.charAt(i));
    }
    arrayReverse = arrayReverse.reverse();
    console.log("Mirror reflection is " + arrayJoin(arrayReverse) + " (reverse way).");}