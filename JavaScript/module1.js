//task 1
let userInput, sumOfNumbers = 0;
for (let i = 0; i < 3; i++) {
    userInput = +prompt("Please, enter a number bigger than 0: ");
    if (userInput > 0) {
        sumOfNumbers += userInput;
    } else if (userInput <= 0) {
        alert("Are you stupid??? Read carefully and enter positive number!");
    } else {
        alert("Oh God! You're so stupid!!!");
    }
}
if(sumOfNumbers === 0) {
    sumOfNumbers = "User didn't enter any positive number.";
}
console.log("Summary of positive numbers = " + sumOfNumbers);


//task 2
let userInputNumber, numbersSummary = 0, numbersMultiply = 1;
for (let i = 0; i < 3;) {
    userInputNumber = +prompt("Please, enter number: ");
    if(userInputNumber >= 0 || userInputNumber < 0) {
        numbersSummary += userInputNumber;
        numbersMultiply *= userInputNumber;
        i++;
    } else {
        alert("You've entered not a number. Please, enter correct number.");
    }
}
if(numbersMultiply > numbersSummary) {
    numbersMultiply += 3;
} else {
    numbersSummary += 3;
}
console.log("Result of summary = " + numbersSummary);
console.log("Result of multiply = " + numbersMultiply);


//task 3
let xCoordinate = +prompt("Enter horizontal coordinate (X): ");
let yCoordinate = +prompt("Enter vertical coordinate (Y): ");
if (xCoordinate === 0 && yCoordinate === 0) {
    alert("The point is in the center.");
} else if (xCoordinate > 0 && yCoordinate > 0) {
    alert("The point is in the first quarter.");
} else if (xCoordinate < 0 && yCoordinate > 0) {
    alert("The point is in the second quarter.");
} else if (xCoordinate < 0 && yCoordinate < 0) {
    alert("The point is in the third quarter.");
} else if (xCoordinate > 0 && yCoordinate < 0) {
    alert("The point is in the forth quarter.");
} else if (xCoordinate > 0 || xCoordinate < 0) {
    alert("The point is on the horizontal axis.");
} else if (yCoordinate > 0 || yCoordinate < 0) {
    alert("The point is on the vertical axis.");
} else {
    alert("You've entered not a number. Refresh your page and try again.");
}


//task 4
let arrayWithNumbers = [];
let arraySize = Math.floor(Math.random() * 20);
for (let i = 0; i < arraySize; i++) {
    arrayWithNumbers[i] = Math.floor(Math.random() * 1000);
}
let minimumElement = arrayWithNumbers[0], maximumElement = arrayWithNumbers[0];
for (let  i = 1; i < arrayWithNumbers.length; i++) {
    if (arrayWithNumbers[i] < minimumElement) {
        minimumElement = arrayWithNumbers[i];
    } else if (arrayWithNumbers[i] > maximumElement) {
        maximumElement = arrayWithNumbers[i];
    }
}
console.log("Array's minimum element = " + minimumElement);
console.log("Array's maximum element = " + maximumElement);
console.log("Index of minimum element = " + arrayWithNumbers.indexOf(minimumElement));
console.log("Index of maximum element = " + arrayWithNumbers.indexOf(maximumElement));


//task 5
let arrayOther = [];
let arrayOtherSize = Math.floor(Math.random() * 20);
for (let i = 0; i < arrayOtherSize; i++) {
    arrayOther[i] = Math.floor(Math.random() * 1000);
}
//First way
let sumofElements = 0;
for (let i = 0; i < arrayOther.length; i++) {
    if(i % 2 === 1) {
        sumofElements += arrayOther[i];
    }    
}
console.log("Summary of elements with odd indexes = " + sumofElements + " (first way).");
//Second way
sumofElements = 0;
for (let i = 1; i < arrayOther.length; i += 2) {
    sumofElements += arrayOther[i];
}
console.log("Summary of elements with odd indexes = " + sumofElements + " (second way).");